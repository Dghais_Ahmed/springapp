package com.example.demo;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HomeController {

    @RequestMapping("/")
    public String index(){
        String style ="<style type='text/css' media='screen'>";
        style += "body { background-color: green; position: fixed; top:50%;left:50%;color:white;font-size:250%;}";
        style += "</style>";

        String message = "Let's Try CI/CD with gitlab";
        String body ="<body>" + message + "</body>";

        return style + body;
    }
}
